from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse
from django.template import loader
from django.views import generic
from .models import Minion

import salt.client
import salt.config
import salt.loader
# Create your views here.
def index(request):
	local = salt.client.LocalClient()
	listaMinions=local.cmd('*', 'test.ping', '')
	context = {'listaMinions':listaMinions}    	
	return render(request, 'indice.html', context)


def detalle2(request, nombre_minion):
	print nombre_minion
	local = salt.client.LocalClient()
	context = {'nombre_minion':nombre_minion}
	return render(request, 'detalle2.html', context)

def detalle3(request, nombre_minion):
	local = salt.client.LocalClient()
	#comando=local.cmd(nombre_minion, 'cmd.run', ['df -h /home'])
	comando=local.cmd(nombre_minion, 'cmd.run', ['reboot'])
	context = {'nombre_minion':nombre_minion, 'comando':comando}
	return render(request, 'detalle3.html', context)
