from django.contrib.auth.models import User
from django import forms


class TextoForm(forms.Form):
   comandoText = forms.CharField(label='comandoText', max_length=100)
class InfoForm(forms.Form):
   infoText = forms.CharField(label='infoText', max_length=100)

class UserForm_Reg(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput)

	class Meta:
		model = User
		fields = ['username', 'email', 'password']

class UserForm_Login(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput)

	class Meta:
		model = User
		fields = ['username', 'password']