from django.conf.urls import url

from . import views
from django.contrib.auth import views as auth_views


app_name = 'directorio'

urlpatterns=[
	url(r'^user/$', views.Usuario, name='Usuario'),
	url(r'^user/register/$', views.User_Reg.as_view(), name='User_Reg'),
	#url(r'^user/login/$', views.User_Login.as_view(), name='User_Login'), 
	url(r'^user/login/$', views.User_Login, name='User_Login'),
	url(r'^user/logout/$', views.User_Logout, name='User_Logout'),
	url(r'^$', views.index, name='index'),
	url(r'^(?P<nombre_minion>\w+)/$', views.Web_Minion, name='Web_Minion'),
	url(r'^(?P<nombre_minion>\w+)/reinicio/$', views.Reboot_Minion, name='Reboot_Minion'),
	url(r'^(?P<nombre_minion>\w+)/comando/$', views.Send_Command, name='Send_Command'),
	url(r'^Reboot_All$', views.Reboot_All, name='Reboot_All'),
	url(r'^(?P<nombre_minion>\w+)/informacion$', views.Info_Minion, name='Info_Minion'),
	url(r'^(?P<nombre_minion>\w+)/informacion/servicio$', views.instalamosNginx, name='instalamosNginx'),
	url(r'^(?P<nombre_minion>\w+)/infoPKG$', views.List_PKG_Minion, name='List_PKG_Minion'),
	url(r'^(?P<nombre_minion>\w+)/infoPKG/status$', views.applyStatus, name='applyStatus'),
	url(r'^(?P<nombre_minion>\w+)/infoPKG/resultado/(?P<nombre_PKG>[\w.:+-]+)$', views.Info_PKG_Minion, name='Info_PKG_Minion'),
	url(r'^(?P<nombre_minion>\w+)/infoPKG/resultado/(?P<nombre_PKG>[\w.:+-]+)/Ins$', views.PKG_Install, name='PKG_Install'),
	url(r'^(?P<nombre_minion>\w+)/infoPKG/resultado/(?P<nombre_PKG>[\w.:+-]+)/Remove$', views.PKG_Remove, name='PKG_Remove'),
	url(r'^(?P<nombre_minion>\w+)/infoPKG/resultado/(?P<nombre_PKG>[\w.:+-]+)$', views.Service_Start, name='Service_Start'),
	url(r'^(?P<nombre_minion>\w+)/infoPKG/resultado/(?P<nombre_PKG>[\w.:+-]+)$', views.Service_Stop, name='Service_Stop'),
	url(r'^(?P<nombre_minion>\w+)/infoPKG/resultado/(?P<nombre_PKG>[\w.:+-]+)$', views.Service_Restart, name='Service_Restart'),
]
