from django.shortcuts import get_object_or_404, render, render_to_response, redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import loader
from django.views import generic
from .models import Minion
from .forms import TextoForm, InfoForm, UserForm_Reg, UserForm_Login


from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.generic import View
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
#from django import forms

import salt.client
import salt.config
import salt.loader

name_User=""

# Create your views here.
@login_required(login_url='user/')
def index(request):
	local = salt.client.LocalClient()
	listaMinions=local.cmd('*', 'test.ping', '')
	    
	context = {'listaMinions':listaMinions, 'name_User':name_User}    	
	return render(request, 'index.html', context)

#recibimos el nombre clicado en indice y le llevamos a la pagina general del minionX
@login_required(login_url='user/')
def Web_Minion(request, nombre_minion):
	local = salt.client.LocalClient()
	comando=local.cmd(nombre_minion, 'cmd.run', ['uname -n'])
	context = {'nombre_minion':nombre_minion, 'comando':comando}
	return render(request, 'Web_Minion.html', context)

#reiniciamos el minion, venimos de pag info del minionX
@login_required(login_url='user/')
def Reboot_Minion(request, nombre_minion):
	local = salt.client.LocalClient()
	#comando=local.cmd(nombre_minion, 'cmd.run', ['df -h /home'])
	comando=local.cmd(nombre_minion, 'cmd.run', ['reboot'])
	context = {'nombre_minion':nombre_minion, 'comando':comando}
	return render(request, 'Reboot_Minion.html', context)

#nos llega el comando introducido, lo mandamos por argumentos y le llevamos a la Info_Command
@login_required(login_url='user/')
def Send_Command(request, nombre_minion):
    if request.method == 'POST':
        form = TextoForm(request.POST)
        if form.is_valid():     
			comando_introducido=form.cleaned_data['comandoText']
			local = salt.client.LocalClient()
			resultado_comando=local.cmd(nombre_minion, 'cmd.run', [comando_introducido])
			context = {'nombre_minion':nombre_minion, 'comando_introducido':comando_introducido, 'resultado_comando':resultado_comando}
			return render(request, 'Info_Command.html', context)

    else:
        form = TextoForm()
	context = {'nombre_minion':nombre_minion, 'form':form}
	return render(request, 'Send_Command.html', context)



#reinicio de todas las maquinas, en el indice
@login_required(login_url='user/')
def Reboot_All(request):
	local = salt.client.LocalClient()
	maquinas=local.cmd('*', 'test.ping', '')
	comando=local.cmd('*', 'cmd.run', ['reboot'])
	context = {'comando':comando, 'maquinas':maquinas}
	return render(request, 'Reboot_All.html', context)

@login_required(login_url='user/')
def Info_Minion(request, nombre_minion):
	local = salt.client.LocalClient()
	memoria_info=local.cmd(nombre_minion, 'disk.usage', [''])
	ip_info = local.cmd(nombre_minion, 'network.ip_addrs', '')
	fecha_info = local.cmd(nombre_minion, 'cmd.run', ['date'])
	#paquetes_info = local.cmd(nombre_minion, 'pkg.list_pkgs', '')
	#info_nginx = local.cmd(nombre_minion, 'service.status', ['nginx'])
	context = {'nombre_minion':nombre_minion, 'ip_info':ip_info,'fecha_info':fecha_info, 'memoria_info':memoria_info}
	return render(request, 'Info_Minion.html', context)

@login_required(login_url='user/')
def instalamosNginx(request, nombre_minion):
	local = salt.client.LocalClient()
	local.cmd(nombre_minion, 'state.apply', ['bin.nginx'])
	info_nginx = local.cmd(nombre_minion, 'service.status', ['nginx'])
	context = {'nombre_minion':nombre_minion, 'info_nginx':info_nginx}
	return render(request, 'detalle8.html', context)

@login_required(login_url='user/')
def applyStatus(request, nombre_minion):
	local = salt.client.LocalClient()
	info_estado=local.cmd(nombre_minion, 'state.apply', ['bin.nginx'])
	context={'nombre_minion':nombre_minion, 'info_estado':info_estado}
	return render(request, 'applyStatus.html', context)

#informacion del paquete
@login_required(login_url='user/')
def List_PKG_Minion(request, nombre_minion):
    if request.method == 'POST':
        form = InfoForm(request.POST)
        if form.is_valid():     
		nombre_PKG=form.cleaned_data['infoText']
		print nombre_PKG
		return HttpResponseRedirect(reverse('directorio:Info_PKG_Minion', args=(nombre_minion, nombre_PKG,)))

    else:
    	local = salt.client.LocalClient()
    	paquetes_info = local.cmd(nombre_minion, 'pkg.list_pkgs', '')
        form = InfoForm()

	context = {'nombre_minion':nombre_minion, 'form':form, 'paquetes_info':paquetes_info }
	return render(request, 'List_PKG_Minion.html', context)

#ejecutamos para ver la info del paquete
@login_required(login_url='user/')
def Info_PKG_Minion(request,nombre_minion, nombre_PKG):
	local = salt.client.LocalClient()
	info_PKG = local.cmd(nombre_minion, 'pkg.info_installed', [nombre_PKG])
	version_PKG = local.cmd(nombre_minion, 'pkg.version', [nombre_PKG])
	info_estado = local.cmd(nombre_minion, 'service.status', [nombre_PKG])
	context={'nombre_minion':nombre_minion,'nombre_PKG':nombre_PKG, 'info_estado':info_estado, 'info_PKG':info_PKG, 'version_PKG':version_PKG}
	return render(request, 'Info_PKG_Minion.html', context)

@login_required(login_url='user/')
def PKG_Install(request,nombre_minion, nombre_PKG):
	local = salt.client.LocalClient()
	info=local.cmd(nombre_minion, 'pkg.install', [nombre_PKG])
	info_PKG = local.cmd(nombre_minion, 'pkg.info_installed', [nombre_PKG])
	context={'nombre_minion':nombre_minion, 'nombre_PKG':nombre_PKG,'info':info,  'info_PKG':info_PKG}
	return render(request, 'InstallPKG.html', context)

@login_required(login_url='user/')
def PKG_Remove(request,nombre_minion, nombre_PKG):
	local = salt.client.LocalClient()
	info=local.cmd(nombre_minion, 'pkg.remove', [nombre_PKG])
	info_PKG = local.cmd(nombre_minion, 'pkg.info_installed', [nombre_PKG])
	context={'nombre_minion':nombre_minion, 'nombre_PKG':nombre_PKG,'info':info, 'info_PKG':info_PKG}
	return render(request, 'RemovePKG.html', context)

def Service_Start(request,nombre_minion, nombre_PKG):
	local = salt.client.LocalClient()
	info_PKG = local.cmd(nombre_minion, 'pkg.info_installed', [nombre_PKG])
	version_PKG = local.cmd(nombre_minion, 'pkg.version', [nombre_PKG])
	info_estado = local.cmd(nombre_minion, 'service.start', [nombre_PKG])
	context={'nombre_minion':nombre_minion,'nombre_PKG':nombre_PKG, 'info_estado':info_estado, 'info_PKG':info_PKG, 'version_PKG':version_PKG}
	return render(request, 'Info_PKG_Minion.html', context)

def Service_Stop(request,nombre_minion, nombre_PKG):
	local = salt.client.LocalClient()
	info_PKG = local.cmd(nombre_minion, 'pkg.info_installed', [nombre_PKG])
	version_PKG = local.cmd(nombre_minion, 'pkg.version', [nombre_PKG])
	info_estado = local.cmd(nombre_minion, 'service.stop', [nombre_PKG])
	context={'nombre_minion':nombre_minion,'nombre_PKG':nombre_PKG, 'info_estado':info_estado, 'info_PKG':info_PKG, 'version_PKG':version_PKG}
	return render(request, 'Info_PKG_Minion.html', context)

def Service_Restart(request,nombre_minion, nombre_PKG):
	local = salt.client.LocalClient()
	info_PKG = local.cmd(nombre_minion, 'pkg.info_installed', [nombre_PKG])
	version_PKG = local.cmd(nombre_minion, 'pkg.version', [nombre_PKG])
	info_estado = local.cmd(nombre_minion, 'service.restart', [nombre_PKG])
	context={'nombre_minion':nombre_minion,'nombre_PKG':nombre_PKG, 'info_estado':info_estado, 'info_PKG':info_PKG, 'version_PKG':version_PKG}
	return render(request, 'Info_PKG_Minion.html', context)

#------------------USUARIOS---------------------------#

def Usuario(request):
	return render(request, 'Usuario.html')


class User_Reg(View):
	form_class = UserForm_Reg

	def get(self, request):
		form = self.form_class(None)
		return render(request, 'registrar.html',{'form':form})

	def post(self, request):
		form = self.form_class(request.POST)

		if form.is_valid():

			user = form.save(commit=False)
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user.set_password(password)
			user.save()
			
			user = authenticate(username=username, password=password)

			if user is not None:
				if user.is_active:
					login(request, user)
					return redirect('directorio:index')

		return render(request, 'registrar.html',{'form':form})




def User_Logout(request):
    logout(request)
    return redirect('directorio:index')




#https://docs.djangoproject.com/en/1.10/topics/auth/default/
def User_Login(request):
    	if request.method == 'POST':
    		print "entro"
        	form = UserForm_Login(request.POST)
        	if form.is_valid():    
				username = form.cleaned_data['username']
				password = form.cleaned_data['password']
				print "continuo" 
				user = authenticate(username=username, password=password)
				if user is not None:
					login(request, user)
					return redirect('directorio:index')

    	else:
        	form = UserForm_Login()


    	context = {'form':form}
	return render(request, 'login.html', context)


#class User_Login(View):
#	form_class = UserForm_Login
#
#	def get(self, request):
#		form = self.form_class(None)
#		return render(request, 'login.html',{'form':form})
#
#	def post(self, request):
#		form = self.form_class(request.POST)
#		if form.is_valid():
#			username = form.cleaned_data['username']
#			password = form.cleaned_data['password']
#			print username, password
#			user = authenticate(username=username, password=password)
#			print user
#			if user is not None:
#					print "entro"
#					login(request, user)
#					return redirect('directorio:index')
#
#		return render(request, 'login.html',{'form':form})




#def registrar_usuario(request):
#	if request.method=='POST':
#		formulario = UserCreationForm(request.POST)
#		if formulario.is_valid:
#			formulario.save()
#			return HttpResponseRedirect('/')
#		else:
#			formulario = UserCreationForm()
#		return render_to_response('registrar.html', {'formulario':formulario}, context_instance=RequestContext(request))

#def registrar_usuario(request):
#   form = UserCreationForm()
# 
 #  if request.method == 'POST':
  #     data = request.POST.copy()
#       errors = form.get_validation_errors(data)
#       if not errors:
#           new_user = form.save(data)
#           return HttpResponseRedirect("/books/")
#   else:
#       data, errors = {}, {}
#
#   return render_to_response("registrar.html", {
#       'form' : forms.FormWrapper(form, data, errors)
#   })

#@login_required(login_url='/accounts/login/')
#def my_view(request):
#    username = request.POST['username']
#    password = request.POST['password']
#    user = authenticate(username=username, password=password)
 #   if user is not None:
  #      login(request, user)
   #     return HttpResponseRedirect('/correcto/')
   #     
   # else:
   # 	return HttpResponseRedirect('/Login.html')

#def logout_view(request):
#    logout(request)
        
        