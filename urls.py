from django.conf.urls import url

from . import views

app_name = 'directorio'

urlpatterns=[
	url(r'^$', views.index, name='index'),
	url(r'^(?P<nombre_minion>\w+)/$', views.detalle2, name='detalle2'),
	url(r'^(?P<nombre_minion>\w+)/detalle/$', views.detalle3, name='detalle3'),
]
